<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Services\ArticleService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ArticleController extends BaseController
{
    public function __construct()
    {
        $this->middleware( 'admin' )->except(['index','show'] );
        return $this->service = new ArticleService();
    }


    /**
     * @param ArticleRequest $request
     * @return JsonResponse
     */
    public function store( ArticleRequest $request ) : JsonResponse
    {
        return response()->json(
            $this->service->create( $request->validated() )->toArray()
        );
    }


    /**
     * @param ArticleRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update( ArticleRequest $request, int $id ) : JsonResponse
    {
        return response()->json(
            $this->service->update( $id, $request->validated() )->toArray()
        );
    }
}
