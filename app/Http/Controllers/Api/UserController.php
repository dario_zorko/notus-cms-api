<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends BaseController
{
    public function __construct()
    {
        $this->middleware('admin');
        return $this->service = new UserService();
    }


    /**
     * @param UserRequest $request
     * @return JsonResponse
     */
    public function store(UserRequest $request ) : JsonResponse
    {
        return response()->json(
            $this->service->create( $request->validated() )->toArray()
        );
    }


    /**
     * @param UserRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UserRequest $request, int $id ) : JsonResponse
    {
        return response()->json(
            $this->service->update( $id, $request->validated() )->toArray()
        );
    }
}
