<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Services\CommentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CommentController extends BaseController
{
    public function __construct()
    {
        $this->middleware( 'moderator' )->except(['index','show','store'] );
        return $this->service = new CommentService();
    }


    /**
     * @param CommentRequest $request
     * @return JsonResponse
     */
    public function store( CommentRequest $request ) : JsonResponse
    {
        return response()->json(
            $this->service->create( $request->validated() )->toArray()
        );
    }


    /**
     * @param CommentRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update( CommentRequest $request, int $id ) : JsonResponse
    {
        return response()->json(
            $this->service->update( $id, $request->validated() )->toArray()
        );
    }
}
