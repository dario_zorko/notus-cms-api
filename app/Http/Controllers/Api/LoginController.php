<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Faker\Provider\Base;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LoginController extends BaseController
{
    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login( LoginRequest $request ) : JsonResponse
    {

        auth()->guard('user')->attempt(['email' => request('email'), 'password' => request('password') ] );

        config(['auth.guards.user.provider' => 'users']);

        $user = User::find( auth()->guard('user')->user()->id );

        $access_token =  $user->createToken('access_token',['user'])->accessToken;

        return response()->json([
            'success' => true,
            'access_token' => $access_token,
            'user' => $user
        ],
            200);

    }

    /**
     * @return JsonResponse
     */
    public function getUser(): JsonResponse
    {
        $user = User::find(auth()->guard('user-api')->user()->id);
        return  response()->json( $user );
    }

    /**
     * @return void
     */
    public function logout(){
        try {
            auth()->guard('user-api')->user()->token()->revoke();
        } catch ( \Exception $exception ){
            dd( $exception );
        }
    }

}
