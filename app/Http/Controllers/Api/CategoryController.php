<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Services\CategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{
    public function __construct()
    {
        $this->middleware( 'admin' )->except( 'index' );
        $this->middleware( 'moderator' )->only( 'index' );
        return $this->service = new CategoryService();
    }


    /**
     * @param CategoryRequest $request
     * @return JsonResponse
     */
    public function store( CategoryRequest $request ) : JsonResponse
    {
        return response()->json(
            $this->service->create( $request->validated() )->toArray()
        );
    }


    /**
     * @param CategoryRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update( CategoryRequest $request, int $id ) : JsonResponse
    {
        return response()->json(
            $this->service->update( $id, $request->validated() )->toArray()
        );
    }
}
