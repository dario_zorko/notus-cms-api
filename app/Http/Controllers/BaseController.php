<?php

namespace App\Http\Controllers;

use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BaseController extends Controller
{

    protected BaseService $service;

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request ) : JsonResponse
    {
        return response()->json(
            $this->service
                ->all( $request->all() )
                ->orderBy( 'id', 'desc' )
                ->get()
        );
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id ) : JsonResponse
    {
        return response()->json(
            $this->service->find( $id )
        );
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id ) : JsonResponse
    {
        return response()->json(
            $this->service->delete( $id )
        );
    }
}
