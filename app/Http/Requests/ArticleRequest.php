<?php

namespace App\Http\Requests;

use App\Rules\Image\UniqueMainImage;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'description' => 'required|string',
            'price' => 'required|numeric|between:0,999999999.99',

            'category_ids' => 'required|array',
            'category_ids.*' => 'required|integer|exists:categories,id',

            'images' => [
                'required',
                'array',
                new UniqueMainImage()
            ],
            'images.*.name' => 'required_if:images.*.src,null|string|max:191',
            'images.*.extension' => 'required_if:images.*.src,null|string|in:jpeg,png,jpg,JPEG,PNG,JPG',
            'images.*.blob' => 'required_if:images.*.src,null|string',
            'images.*.path' => 'nullable|string|exists:images,path',
            'images.*.main' => 'nullable|boolean',
        ];
    }
}
