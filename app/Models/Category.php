<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public $fillable = [];
    public $guarded = [];
    public $hidden = [];

    public function articles()
    {
        return $this->belongsToMany( Article::class, 'article_category', 'category_id', 'article_id' );
    }

    public function parent()
    {
        return $this->belongsTo( Category::class, 'parent_id' );
    }

    public function parents()
    {
        return $this->belongsTo( Category::class, 'parent_id' )
            ->with(['parents']);
    }

    public function child()
    {
        return $this->belongsTo( Category::class, 'parent_id' );
    }

    public function children()
    {
        return $this->belongsTo( Category::class, 'parent_id' )
            ->with(['children']);
    }
}
