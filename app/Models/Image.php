<?php

namespace App\Models;

use App\Casts\ImagePathCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $table = 'images';
    protected $primaryKey = 'path';
    public $incrementing = false;
    public $timestamps = false;

    public $fillable = [];
    public $guarded = [];
    public $hidden = [];

//    protected $casts = [
//        'path' => ImagePathCast::class
//    ];

    public function article()
    {
        return $this->belongsTo( Article::class, 'article_id' );
    }
}
