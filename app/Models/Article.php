<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $table = 'articles';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public $fillable = [];
    public $guarded = [];
    public $hidden = [];

    public function images()
    {
        return $this->hasMany( Image::class, 'article_id' );
    }

    public function categories()
    {
        return $this->belongsToMany( Category::class, 'article_category', 'article_id', 'category_id' );
    }

    public function comments()
    {
        return $this->hasMany( Comment::class, 'article_id' );
    }
}
