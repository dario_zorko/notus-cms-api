<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $table = 'comments';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public $fillable = [];
    public $guarded = [];
    public $hidden = [];

    public function article()
    {
        return $this->belongsTo( Article::class, 'article_id' );
    }
}
