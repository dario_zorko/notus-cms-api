<?php

namespace App\Rules\Image;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class UniqueMainImage implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if( !is_array( $value ) )
            $fail( 'Only one ' . $attribute . ' must be selected as main ' );

        $main = 0;
        foreach ( $value as $image ){
            if ( isset( $image['main'] ) && (boolean)$image['main'] )
                $main ++;
        }

        if( $main !== 1 )
            $fail( 'Only one ' . $attribute . ' must be selected as main ' );
    }
}
