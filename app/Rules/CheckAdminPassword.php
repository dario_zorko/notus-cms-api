<?php

namespace App\Rules;

use App\Models\User;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Facades\Hash;

class CheckAdminPassword implements ValidationRule
{
    private string $email;
    public function __construct( string $email )
    {
        $this->email = $email;
    }
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if( !strlen( $this->email ) )
            $fail( 'Incorrect password' );
        $user = User::where( 'email', $this->email )->first();
        if( !( $user && Hash::check( $value, $user->password ) ) )
            $fail( 'Incorrect password' );
    }
}
