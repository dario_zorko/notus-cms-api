<?php

namespace App\Rules\Category;

use App\Models\Category;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class CheckLevel implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $category = Category::with(['parents'])->find( $value );

        if ( $category->parents && $category->parents->parents )
            $fail('The maximum level of subcategory is 3.' );
    }


}
