<?php

namespace App\Services;

use App\Models\Comment;

class CommentService extends BaseService implements BaseServiceInterface
{
    public function __construct()
    {
        $this->model = new Comment();
        parent::__construct();
    }
}

