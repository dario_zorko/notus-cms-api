<?php

namespace App\Services;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;

class CategoryService extends BaseService implements BaseServiceInterface
{
    public function __construct()
    {
        $this->model = new Category();
        parent::__construct();
    }

    /**
     * @param array $request
     * @return Builder
     */
    public function all(array $request = [] ) : Builder {
        return $this->model->query()
            ->with([
                'parents'
            ]);
    }
}
