<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class BaseService
{
    public Model $model;

    public function __construct(){}

    /**
     * @param array $request
     * @return Builder
     */
    public function all(array $request = [] ) : Builder {
        return $this->model->query();
    }

    /**
     * @param int $id
     * @return Model
     */
    public function find(int $id ) : Model {
        return $this->model->find( $id );
    }

    /**
     * @param array $data
     * @return Model
     */
    public function create(array $data ) : Model {
        $item = $this->model->create( $data );
        return $this->find( $item->id );
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model
     */
    public function update(int $id, array $data ) : Model {
        $item = $this->model->find( $id )->update( $data );
        return $this->find( $id );
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id ) : bool {
        return $this->model->find( $id )->delete();
    }
}
