<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

interface BaseServiceInterface
{
    /**
     * @set model
     */
    public function __construct();

    /**
     * @param array $filters
     * @return Builder
     */
    public function all(array $request ) : Builder;

    /**
     * @param int $id
     * @return Model
     */
    public function find(int $id ) : Model;

    /**
     * @param array $data
     * @return Model
     */
    public function create(array $data ) : Model;

    /**
     * @param int $id
     * @param array $data
     * @return Model
     */
    public function update(int $id, array $data ) : Model;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id ) : bool;
}
