<?php

namespace App\Services;

use App\Models\Article;
use App\Models\User;

class UserService extends BaseService implements BaseServiceInterface
{
    public function __construct()
    {
        $this->model = new User();
        parent::__construct();
    }
}

