<?php

namespace App\Services;

use App\Models\Article;
use App\Models\Image;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ArticleService extends BaseService implements BaseServiceInterface
{
    public function __construct()
    {
        $this->model = new Article();
        parent::__construct();
    }



    public function all(array $request = []): Builder
    {
        return $this->model->query()
            ->with([
                'images',
                'comments',
                'categories' => function( $q ) {
                    $q->with([
                        'parents'
                    ]);
                }
            ]);
    }



    public function find(int $id): Model
    {
        return $this->model->query()
            ->with([
                'images',
                'comments',
                'categories' => function( $q ) {
                    $q->with([
                        'parents'
                    ]);
                }
            ])
            ->findOrFail( $id );
    }



    public function create(array $data): Model
    {
        $images = $data['images'];
        unset( $data['images'] );
        $category_ids = $data['category_ids'];
        unset( $data['category_ids'] );

        $article = $this->model->query()->create($data);

        $article->categories()->attach( $category_ids );

        $new_images = [];
        foreach ( $images as $image ){
            $new_images[] = $this->imageToStorage( $image );
        }
        $article->images()->createMany( $new_images );

        return $this->find( $article->id );
    }



    public function update(int $id, array $data): Model
    {
        $images = $data['images'];
        unset( $data['images'] );
        $category_ids = $data['category_ids'];
        unset( $data['category_ids'] );

        $article = $this->model->query()->findOrFail( $id );
        $article->update( $data );

        $article->categories()->sync( $category_ids );

        $new_images = [];
        $existing_image_path = [];
        $main_image = [];
        foreach ( $images as $image ){
            if( isset( $image['path'] ) && $image['path'] ){
                $existing_image_path[] = $image['path'];
            } else {
                $new_images[] = $this->imageToStorage( $image );
            }
            if( $image['main'] )
                $main_image = $image;
        }
        $article->images()->update( ['main' => 0] );
        $article->images()->whereNotIn( 'path', $existing_image_path )->delete();
        $article->images()->createMany( $new_images );
        $article->images()->where( 'path', $main_image['path'] )->update( ['main' => 1] );

        return $this->find( $id );
    }



    public function delete(int $id): bool
    {
        $article = $this->model->query()->findOrFail( $id );
        $article->categories()->detach();
        $article->images()->delete();
        return $article->delete();
    }



    private function imageToStorage( array $image ) : array
    {
        $file_base64 = explode( ',', $image['blob'] )[1];
        $name = \Illuminate\Support\Str::random(32 );
        while( true ){
            if( Image::where( 'path', 'images/' . $name . '.' . $image['extension'] )->first() )
                $name = \Illuminate\Support\Str::random(32 );
            else
                break;
        }
        Storage::put( 'public/images/' . $name . '.' . $image['extension'], base64_decode( $file_base64 ) );
        return [
            'path' =>'images/' . $name . '.' . $image['extension'],
            'name' => $image['name'],
            'main' => $image['main']
        ];
    }
}
