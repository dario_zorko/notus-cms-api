<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('images', function (Blueprint $table) {
            $table->string( 'path' );
            $table->string( 'name' );
            $table->boolean( 'main' );
            $table->unsignedBigInteger( 'article_id' );
            $table->foreign( 'article_id' )->references( 'id' )->on('articles' );
            $table->primary(['path']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('images');
    }
};
