- composer install
- php artisan migrate --seed
- php artisan passport:install
- php artisan config:cache

- admin: admin@gmail.com / password
- moderator: moderator@gmail.com / password
