<?php

use App\Http\Controllers\Api\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/admin/login',[LoginController::class, 'login'] );

Route::group( [ 'prefix' => 'admin', 'middleware' => ['auth:user-api','scopes:user'] ], function() {

    Route::get( 'logout', [ LoginController::class, 'logout']);
    Route::get( 'get-user', [ LoginController::class, 'getUser']);

    Route::namespace('\App\Http\Controllers\Api')->group( function () {
        Route::apiResource( 'article', 'ArticleController' );
        Route::apiResource( 'category', 'CategoryController' );
        Route::apiResource( 'comment', 'CommentController' );
        Route::apiResource( 'user', 'UserController' );
    } );

} );


// front endpoints
Route::group( [ 'prefix' => 'web'], function () {

    Route::get( '/article', 'ArticleController@index' );
    Route::get( '/article/{id}', 'ArticleController@index' );
    Route::post( '/comment', 'CommentController@store' );
} );





